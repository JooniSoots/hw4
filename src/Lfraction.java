import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 * viited: http://www.guideforschool.com/2469030-java-program-to-find-the-gcd-of-two-numbers-division-method/
 * http://codereview.stackexchange.com/questions/83245/mathematical-functions-on-fractions
 * http://enos.itcollege.ee/~jpoial/algoritmid/Lfraction/index.html
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
	   /*Lfraction f1 = new Lfraction (2, 4);
	   Lfraction f3 = new Lfraction (1, 4);
	   System.out.println(f1.divideBy(f3));
	   System.out.println(f1.plus(f3));
	   System.out.println(f1.equals(f3));
	   System.out.println(f1.compareTo(f3));
	   System.out.println ("num: " + f1.getNumerator());
       System.out.println ("denom: " + f3.getDenominator());
	   System.out.println(f1.equals (f3));
	   System.out.println(valueOf(""));
	   
	   
	   try {
		System.out.println(f1.clone());
	} catch (CloneNotSupportedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
   }

   // TODO!!! instance variables here
   private long num;
   private long den;
   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      // TODO!!!
	   if (b == 0) {
		   throw new RuntimeException ("Nimetaja ei tohi v�rduda 0-ga");
	   
	   }
	   // muudame nimetaja positiivseks arvuks
	   if (b < 0){
		   b *= -1;
		   a *= -1;
	   }
	   this.num = a;
	   this.den = b;
	   
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.num; // TODO!!!
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.den; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
     
      return String.valueOf(this.getNumerator()) + "/" + String.valueOf(this.getDenominator());
      // TODO!!!
   }
   /** Finding greatest common denominator.
    * 
    * @return gcd
    */
   static long gcd(long n1, long n2) {
      long r = n2;
      while (n2 != 0) {
         r = n2;
         n2 = n1 % n2;
         n1 = r;
      }
      return n1;
   }

   /** Reduce the fraction by gcd.
    * 
    * @return
    */
   public Lfraction reduce () {
	      long common = 0;
	      
	      long num1 = Math.abs(this.getNumerator());
	      long den1 = Math.abs(this.getDenominator());
	      
	      if (num1 > den1) common = gcd(num1, den1);
	      else if (num1 < den1) common = gcd(den1, num1);
	      else common = num1;
	      
	      long num = this.getNumerator() / common;
	      long den = this.getDenominator() / common;
	      return new Lfraction(num, den);
	   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
	   Lfraction Lf2 = (Lfraction) m;
	   if (Lf2.getDenominator() == this.getDenominator() ) {
		   return this.getNumerator() == Lf2.getNumerator();
	   }
	   else {
		   double diff = Lf2.getDenominator() / this.getDenominator();
		   return (this.getNumerator() * diff) == Lf2.getNumerator();
	   }// TODO!!!
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.getNumerator(), this.getDenominator()); // TODO!!!
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
	  if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		  throw new RuntimeException("Murdude liitmisel nimetaja ei tohi olla 0.");   
      return new Lfraction ((this.getNumerator() * m.getDenominator()) + (this.getDenominator() * m.getNumerator()), (this.getDenominator() * m.getDenominator()) ).reduce(); // TODO!!!
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
	  if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		  throw new RuntimeException("Murdude korrutamisel nimetaja ei tohi olla 0.");   
	  return new Lfraction(this.getNumerator() * m.getNumerator() , this.getDenominator() * m.getDenominator()).reduce(); // TODO!!!
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
	   if (this.getNumerator() == 0) {
			throw new ArithmeticException("Murrust: \" " + this + " \"ei saa p��rdv��rtust v�tta. Nimetaja ei tohi olla 0.");
	   }
      return new Lfraction(this.getDenominator() , this.getNumerator()); // TODO!!!
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
	  if (this.getDenominator()==0) 
		  throw new RuntimeException("Nimetaja ei tohi olla 0.");
      return new Lfraction(-this.getNumerator(),this.getDenominator()); // TODO!!!
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
	  if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		  throw new RuntimeException("Murdude lahutamisel nimetaja ei tohi olla 0.");   
      return new Lfraction ((this.getNumerator() * m.getDenominator()) - (this.getDenominator() * m.getNumerator()), (this.getDenominator() * m.getDenominator())).reduce(); // TODO!!!
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   
   public Lfraction divideBy (Lfraction m) {
	   if (this.getDenominator()==0 || m.getDenominator()==0){
		   throw new RuntimeException("Tehet "+this+"/"+m+" ei saa teostada. Nimetaja ei tohi olla 0.");
	   }
      return new Lfraction ((this.getNumerator() * m.getDenominator()), (this.getDenominator() * m.getNumerator())).reduce(); // TODO!!!
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {// TODO!!!
	   if (this.equals(m)){
		   return 0;
	   } else if (this.getNumerator() * m.getDenominator() < this.getDenominator() * m.getNumerator() ){
		   return -1;
	   } else {
		   return 1;
	   }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
	  Lfraction uus = new Lfraction (this.getNumerator() , this.getDenominator());  
      return uus; // TODO!!!
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.getNumerator() / this.getDenominator(); // TODO!!!
      // 
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
	  long f = this.getNumerator()%this.getDenominator();
	  return new Lfraction( f, this.getDenominator());
	   
      // TODO!!!
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) this.getNumerator()/ this.getDenominator(); // TODO!!!
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
	   if (d == 0){
		   throw new RuntimeException("Arvu "+ f +"murruks viimisel ei tohi nimetaja olla 0.");
	   }
	 return new Lfraction (Math.round( f * d), d); // TODO!!!
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
	   if (s == null || s.trim().length() == 0)
		   throw new RuntimeException("S�ne \"" + s + "\" on t�hi v�i sisaldab ainult t�hikuid");
	   if (s.indexOf('/') < 0)
	       throw new RuntimeException("S�nes " + s + " on puudu / m�rk");
	   String[] osad = s.split("/");
	   if (osad.length > 2) 
		   throw new RuntimeException("S�ne \"" + s + "\" ei kujuta murdu, liiga palju argumente.");
	   if (osad.length < 2) 
		   throw new RuntimeException("S�ne \"" + s + "\" ei kujuta murdu, liiga v�he argumente.");
	   try {
			Long.parseLong(osad[0]);
			Long.parseLong(osad[1]);
	   } catch (NumberFormatException e) {
			throw new RuntimeException("S�ne \"" + s + "\" ei sobi murruks, sest sisaldab keelatud s�mboleid.");
	   } catch (Exception f) {
			throw new RuntimeException("S�ne \"" + s + "\" ei sobi murruks, sest puudub nimetaja ja/v�i lugeja.");
       }
	   if (Long.parseLong(osad[1]) == 0) {
			throw new ArithmeticException("S�ne \"" + s + "\" ei sobi murruks, sest murru nimetaja ei tohi olla 0.");
	   }
	   
	  return new Lfraction(Long.parseLong(osad[0]), Long.parseLong(osad[1]));
     // TODO!!!
   }
}

